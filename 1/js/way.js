AFRAME.registerComponent('way', {
    schema: {
        width: { default: 1, min: 0 },
        vertices: { type: 'string' }
    },

    init: function() {
        const vertices = this.data.vertices.split(",")
            .map( vcoords => vcoords
                                .trim()
                                .split(" ")
                                .map( xyz => parseFloat(xyz))
                );
        const realVertices = [];
        const faces = [];
        let dx, dz, len, dxperp, dzperp;
        const k = vertices.length-1;
        for(let i=0; i<k; i++) {
            dx = vertices[i+1][0] - vertices[i][0];
            dz = vertices[i+1][2] - vertices[i][2];
            len = distance(vertices[i], vertices[i+1]);
            dxperp = -(dz * (this.data.width/2)) / len;
            dzperp = dx * (this.data.width/2) / len;
            realVertices.push (new THREE.Vector3(
                vertices[i][0] - dxperp, 
                vertices[i][1], 
                vertices[i][2] - dzperp)
            );
            realVertices.push (new THREE.Vector3(
                vertices[i][0] + dzperp, 
                vertices[i][1], 
                vertices[i][2] + dzperp)
            );
        }
        realVertices.push (new THREE.Vector3(
                vertices[k][0] - dxperp, 
                vertices[k][1], 
                vertices[k][2] - dzperp )
        );
        realVertices.push (new THREE.Vector3(
            vertices[k][0] + dzperp, 
            vertices[k][1], 
            vertices[k][2] + dzperp )
        );

        for(let i=0; i<k; i++)     {
            faces.push(new THREE.Face3(i*2, i*2+1, i*2+2));
            faces.push(new THREE.Face3(i*2+1, i*2+3, i*2+2));
        }

        const geom = new THREE.Geometry();
        geom.vertices = realVertices;
        geom.computeBoundingBox();
        geom.faces = faces;
        geom.mergeVertices();
        geom.computeFaceNormals();
        geom.computeVertexNormals();

        const mesh = new THREE.Mesh(geom); 
        this.el.setObject3D("mesh", mesh);
    }
});

function distance(v1,v2) {
    const dx = v2[0] - v1[0];
    const dy = v2[1] - v1[1];
    const dz = v2[2] - v1[2];
    return Math.sqrt(dx*dx + dy*dy + dz*dz);
}
