const Dialog = require('../../common/Dialog');

window.onload = function() {
    const parts = window.location.href.split('?');
    const get = { lat: 51.05, lon: -0.72 };

    if(parts.length==2) {
        if(parts[1].endsWith('#')) { 
            parts[1] = parts[1].slice(0, -1);
        }
        var params = parts[1].split('&');
        for(var i=0; i<params.length; i++) {
            var param = params[i].split('=');
            get[param[0]] = param[1];
        }
    }

    const dlg = new Dialog(document.body,
        { 'OK': ()=> { 
            document.getElementById('dem1').setAttribute('terrarium-dem',
                 { lat: document.getElementById('lat').value, 
                    lon: document.getElementById('lon').value  }
            );
            dlg.hide();
        }},
        { top: 'calc(50% - 150px)',
        left: 'calc(50% - 150px)',
        width: '300px',
        position: 'absolute',
        backgroundColor: 'white',
        color: 'black',
        padding: '10px'
        });
    const div = document.createElement("div");
    const h1 = document.createElement("h1");
    h1.appendChild(document.createTextNode("OSM and A-Frame demo"));
    const p = document.createElement("p");
    p.appendChild(document.createTextNode(
                'This demo renders OSM data in 3D superimposed over '+
                'Terrarium DEM data. Please note you need a '+
                'decent graphics card; the demo may crash '+
                'otherwise. Note there are various limitations, for ' +
                'instance (due to the way A-Frame text is handled) ' +
                'non-ASCII characters are not rendered, and sea or rivers '+
                'are currently not shown.')
    );
    const lblLat = document.createTextNode("Latitude:");
    const txtLat = document.createElement("input");
    txtLat.id = 'lat';
    txtLat.type = 'number';
    txtLat.min = -90;
    txtLat.max = 90;
    txtLat.value = get.lat;
    const lblLon = document.createTextNode("Longitude:");
    const txtLon = document.createElement("input");
    txtLon.id = 'lon';
    txtLon.type = 'number';
    txtLon.value = get.lon;
    txtLon.min = -180;
    txtLon.max =  180;
    div.appendChild(h1);
    div.appendChild(p);
    div.appendChild(lblLat);
    div.appendChild(document.createElement("br"));
    div.appendChild(txtLat);
    div.appendChild(document.createElement("br"));
    div.appendChild(lblLon);
    div.appendChild(document.createElement("br"));
    div.appendChild(txtLon);
    div.appendChild(document.createElement("br"));
    dlg.setDOMContent(div);
    dlg.show();
};
