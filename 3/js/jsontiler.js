const Tiler = require('./tiler');

class JsonTiler extends Tiler {
    constructor(url) {
        super(url);
    }

    async readTile(url) {
        const response = await fetch(url);
        const data = await response.json();
        console.log(`JsonTiler.readTile() returning..`);
        return data;
    }
}

module.exports = JsonTiler;
