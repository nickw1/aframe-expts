AFRAME.registerComponent('position-manager', {

    init: function() {
        this.lastPosition = { x:0, y:0, z:0 };
    },
    
    tick: function(time, delta) {
        this.newPosition = this.el.getAttribute('position');

        const dx = this.newPosition.x - this.lastPosition.x;
        const dy = this.newPosition.y - this.lastPosition.y;
        const dz = this.newPosition.z - this.lastPosition.z;
        const dist = Math.sqrt(dx*dx + dy*dy +dz*dz);

        if(dist>=0.0001) {
            this.el.sceneEl.emit('cameramoved', {
                x: this.newPosition.x,     
                y: this.newPosition.y, 
                z: this.newPosition.z
            });
        }

        this.lastPosition = { x: this.newPosition.x, 
                          y: this.newPosition.y, 
                          z: this.newPosition.z }; 
    }
});
