const THREE = require('three');
const CameraControl = require('./cameracontrol');
const Terrarium = require('./terrarium');
const OSM3D = require('./osm3d');



var scene;
var camera;

var renderer ;
var cameraControl;

renderer = new THREE.WebGLRenderer({antialias: true});

init();
animate();


function init() {
    const parts = window.location.href.split('?');
    const get = { lat: 51.05, lon: -0.72 };

    if(parts.length==2) {
        if(parts[1].endsWith('#')) { 
            parts[1] = parts[1].slice(0, -1);
        }
        var params = parts[1].split('&');
        for(var i=0; i<params.length; i++) {
            var param = params[i].split('=');
            get[param[0]] = param[1];
        }
    }
    scene = new THREE.Scene();
    const light = new THREE.AmbientLight(new THREE.Color(0xbbbbbb).convertSRGBToLinear(), 1);
    const light2 = new THREE.DirectionalLight(new THREE.Color(0xe0e0e0).convertSRGBToLinear(), 0.6);
    light2.position.set(-0.5, 0.5, 1);
    scene.add(light);
    scene.add(light2);
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.01, 50000);
    cameraControl = new CameraControl(camera, 5);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor( 0x87ceeb );
    renderer.shadowMap.enabled = true;
    document.body.appendChild(renderer.domElement); 
    cameraControl.setup();
    
    const osm = new OSM3D(scene, 'https://www.hikar.org/fm/ws/tsvr.php?way=highway&poi=place,natural&x={x}&y={y}&z={z}');
    const terrarium = new Terrarium(scene, {
            yExag: 3,
            color: '#00c000',
            opacity: 1.0,
            zoom:13,
            url: 'proxy.php?x={x}&y={y}&z={z}'
        });
    terrarium.on("positionset", cameraPos=> {
        camera.position.x = cameraPos.x;
        camera.position.y = cameraPos.y;
        camera.position.z = cameraPos.z;
    });
    terrarium.on("dataloaded", results=> {
        if(results.elevation >= 0) {
            camera.position.y = results.elevation + 1.6*results.yExag;
        }
        osm.loadDem(results);
    });
    cameraControl.on("cameramoved", terrarium.onCameraMove.bind(terrarium));
    cameraControl.on("cameramove", terrarium.getElevationFromCameraPos.bind(terrarium));
    terrarium.setPosition(parseFloat(get.lon), parseFloat(get.lat), 1000);

    window.addEventListener("resize", e=> {
        renderer.setSize(window.innerWidth, window.innerHeight);
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
    });
}

function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    cameraControl.moveCamera();
}
