const Tile=require('./Tile');
const GoogleProjection = require('./GoogleProjection');
const PNGReader = require('./pngjs/PNGReader');
const DEM = require("./dem");
const THREE = require('three');
const OsmWay = require('./osmway');
const DemTiler = require('./demtiler');
const Adjuster = require('./adjuster');
const Eventable = require('./Eventable');

class Terrarium extends Eventable {

       constructor(scene, options) {
        super();
        this.scene = scene;
        this.options = options;
        this.tilesLoaded = [];
        this.tiler = new DemTiler();
        this.adjuster = new Adjuster(this.tiler);
        this.dems = {};
        this.firstLoad = true;
    }

    setTilerUrl(url) {
        this.tiler.url = url;
    }

    setTilerZoom(zoom) {
        this.tiler.setZoom(zoom);
    }

    setParams(yExag) {
        this.adjuster.yExag = yExag;
    }

    setDefaultElevation(defaultElevation) {
        this.adjuster.defaultElevation = defaultElevation;
    }

    async updateLonLat(lon, lat) {
        const sphMerc = this.tiler.lonLatToSphMerc(lon,lat);
        return await this.updateSphMerc(sphMerc);
    }

    async updateSphMerc(sphMerc) {
        document.getElementById('loadingDataMsg').style.visibility = 'visible';
        const dems = [];
        const newData = await this.tiler.update(sphMerc);
        newData.forEach ( data=> { 
            const dem = this.loadTerrariumData(data);
            if(dem != null) {
                dems.push(dem);
            }
        });     
        document.getElementById('loadingDataMsg').style.visibility = 'hidden';
        return dems;
    }

    loadTerrariumData(data) {
        let demData = null;    
        if(data !== null) {
            const geom = this.createDemGeometry(data);
            geom.geom.computeFaceNormals();
            geom.geom.computeVertexNormals();
            const mesh = new THREE.Mesh(geom.geom, new THREE.MeshLambertMaterial({
                color: this.color,
                opacity: this.opacity
            }));
            this.scene.add(mesh);
            const dem = new DEM(geom.geom, geom.realBottomLeft);
            this.dems[`${data.tile.z}/${data.tile.x}/${data.tile.y}`] = dem;
            demData = { dem: dem, tile: data.tile};
        }
        return demData;
    }

    createDemGeometry(data) {
        const demData = data.data;
        const tile = data.tile; 
        const topRight = tile.getTopRight();
        const bottomLeft = tile.getBottomLeft();
        const centre = [(topRight[0] + bottomLeft[0]) / 2, 
            (topRight[1] + bottomLeft[1]) /2];
        const xSpacing = (topRight[0] - bottomLeft[0]) / (demData.w-1);
        const ySpacing = (topRight[1] - bottomLeft[1]) / (demData.h-1);
        const realBottomLeft = [bottomLeft[0], bottomLeft[1]] ;
        const geom = new THREE.PlaneBufferGeometry(topRight[0] - bottomLeft[0], topRight[1] - bottomLeft[1], demData.w - 1,  demData.h - 1);
        const array = geom.getAttribute("position").array;
        let i;
        for (let row=0; row<demData.h; row++) {
           for(let col=0; col<demData.w; col++) {
                i = row*demData.w + col;
                array[i*3+2] = -(centre[1] + array[i*3+1]); 
                array[i*3+1] = demData.elevs[i] * this.adjuster.yExag;
                array[i*3] += centre[0];
            }        
        }

        return {geom: geom, realBottomLeft: realBottomLeft };    
    }

    getElevation(lon, lat, z) {
        const sphMercPos = this.tiler.lonLatToSphMerc(lon, lat, z);
        return this.getElevationFromSphMerc(sphMercPos, z);
    }

    getElevationFromSphMerc(sphMercPos, z) {    
        const tile = this.tiler.getTile(sphMercPos, z);
        if(this.dems[`${tile.z}/${tile.x}/${tile.y}`]) {
            const scaled = [ sphMercPos[0], sphMercPos[1]  ];
            return this.dems[`${tile.z}/${tile.x}/${tile.y}`].getHeight
                (scaled[0], scaled[1]);
        }
        return -1;
    }

    async setPosition(lon, lat, elevation) {
        this.setTilerUrl(this.options.url);
        this.setTilerZoom(this.options.zoom);
        this.color = this.options.color;
        this.opacity = this.options.opacity;
        this.setDefaultElevation(this.options.defaultElevation);
        this.setParams(this.options.yExag);
        if(lon >= -180 && lon <= 180 && lat >= -90 && lat <= 90) {
            const cameraPos = this.adjuster.lonLatToCamera(lon, lat);
            this.eventHandlers.positionset(cameraPos);
            const demData = await this.updateLonLat(lon, lat);
            this.firstLoad = false;
            this.notifyDemLoaded(demData, true, lon, lat);
        }
    }

    async onCameraMove(cameraPos) {
        const sphMerc = this.adjuster.cameraToSphMerc(cameraPos);
        const demData = await this.updateSphMerc(sphMerc);
        this.notifyDemLoaded(demData, false);
    }

    notifyDemLoaded(demData, moveToGround, lon, lat) {
        this.eventHandlers.dataloaded({
            demData: demData,
            yExag: this.adjuster.yExag,
            elevation: moveToGround ? this.getElevation(lon, lat, this.options.zoom)+10*this.adjuster.yExag: -1 
        });
    }


    getElevationFromCameraPos(cameraPos) {
        const sphMercPos = this.adjuster.cameraToSphMerc(cameraPos);
        const h = this.getElevationFromSphMerc(sphMercPos, this.options.zoom);
        return h;
    }
}

module.exports = Terrarium;
