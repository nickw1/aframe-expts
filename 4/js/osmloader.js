const OsmWay = require('./osmway');

class OsmLoader {
    constructor(system) {
        this.system = system;
        this.drawProps = { 'footway' : { color: '#ffff00', width: 5 },
             'path' : { color: '#ffff00', width: 5 },
             'steps' : { color: '#ffff00', width: 5 },
             'bridleway' : { color: '#ff8080', width: 5 },
             'byway' : { color: '#ff8080', width: 10 },
            'track' :  { color: '#ff8080', width: 10 },
            'cycleway' : { color: '#00ffff', width: 5 },
            'residential' : { width: 10 },
            'unclassified' : { width: 15 },
            'tertiary' :  { width: 15 },
            'secondary' : { width: 20 },
            'primary' : { width : 30 },
            'trunk' : { width: 30 },
            'motorway' : { width: 60 }
        }
    }

    loadOsm(osmData, tileid, dem=null) {
        osmData.features.forEach  ( (f,i)=> {
            const line = [];
            if(f.geometry.type=='LineString' && f.geometry.coordinates.length >= 2) {
                f.geometry.coordinates.forEach (coord=> {
            
                        const h = 
                            dem? dem.getHeight(
                                    coord[0],
                                    coord[1]
                                ) + this.system.adjuster.yExag*2: 0;
                       line.push([coord[0],
                                h,
                                -coord[1]]);
               });
                    
                
                const g = this.makeWayGeom(line,     
                        (this.drawProps[f.properties.highway] ? 
                            (this.drawProps[f.properties.highway].width || 5) :
                         5));

                const color = this.drawProps[f.properties.highway] ? 
                    (this.drawProps[f.properties.highway].color||'#ffffff') : 
                    '#ffffff';
            
                const mesh = new THREE.Mesh(g, new THREE.MeshBasicMaterial(
                    {color:color}
                    ));
                        
                this.system.el.setObject3D(`${tileid}:${f.properties.osm_id}`, mesh);
            }  else if (f.geometry.type=='Point' && f.properties.name) {
                const h = dem ? 
                    dem.getHeight(f.geometry.coordinates[0],
                        f.geometry.coordinates[1]): 0;

                const p = { 
                    x:  f.geometry.coordinates[0],
                    y: h+20*this.system.adjuster.yExag,
                    z: -f.geometry.coordinates[1] 
                };

                const textEntity=document.createElement("a-entity");
                textEntity.setAttribute("text", {
                    value: f.properties.name,
                });

                textEntity.setAttribute('position', {
                    x:p.x,
                    y:p.y,
                    z:p.z
                });
                textEntity.setAttribute('scale', {
                    x:1000,
                    y:1000,
                    z:1000
                });

                //textEntity.setAttribute('rotation', { y:180*i });
                textEntity.setAttribute('look-at', '[camera]');

                this.system.el.appendChild(textEntity);
            }
        }); 
    }

    makeWayGeom(vertices, width=1) {
        return new OsmWay(vertices, width).geometry;
    }
}

module.exports = OsmLoader;
