const OsmLoader = require('./osmloader');
const Adjuster = require('./adjuster');
const THREE = require('three');

class OSM3D {
    constructor(scene, url) {
        this.tilesLoaded = [];
        this.osmLoader = new OsmLoader(this);
        this.adjuster = new Adjuster();
        this.scene = scene;
        this.url = url;
    }

    async loadData(dem) {
        const tileIndex = `${dem.tile.z}/${dem.tile.x}/${dem.tile.y}`;
        if(this.tilesLoaded.indexOf(tileIndex) == -1) {
            const realUrl = this.url.replace('{x}', dem.tile.x)
                                .replace('{y}', dem.tile.y)
                                .replace('{z}', dem.tile.z);
            const response = await fetch(realUrl);
            const osmData = await response.json();
            this.tilesLoaded.push(tileIndex);
            return osmData;
        }
        return null;
    }

    async applyDem(osmData, dem) {
        await this.osmLoader.loadOsm(osmData,`${dem.tile.z}/${dem.tile.x}/${dem.tile.y}`, dem.dem);
    }

    async loadDem(detail) {
        this.adjuster.yExag = detail.yExag;
        for(let i=0; i<detail.demData.length; i++) {
            const osmData = await this.loadData(detail.demData[i]);
            await this.applyDem(osmData, detail.demData[i]);
        }
    }

    addWayMesh(id, mesh) {
        this.scene.add(mesh);
    }

    // referenced stemkoski.github.io/Three.js/Sprite-Text-Labels.html
    makeText(text, style, p) {
        const canvas = document.createElement('canvas');
        canvas.width = 512; 
        canvas.height = 256; 
        const ctx = canvas.getContext('2d');
        ctx.font = `bold ${style.fontSize}px ${style.fontFamily}`;
        const metrics = ctx.measureText(text);
        const width = metrics.width + 20;
        ctx.fillStyle = style.backgroundColor;
        const height = style.fontSize * 1.5;
        ctx.fillRect(0, 0, width, height);
        ctx.fillStyle =  style.color;
        ctx.fillText(text, 10, style.fontSize); 
        const texture = new THREE.Texture(canvas);
        texture.needsUpdate = true;
        const spriteMaterial = new THREE.SpriteMaterial({
            map: texture
        });
        const sprite = new THREE.Sprite(spriteMaterial);
        sprite.scale.set(500, 250, 1.0);
        sprite.position.set(p.x, p.y, p.z);
        this.scene.add(sprite);
    }
}

module.exports = OSM3D;
