const OsmLoader = require('./osmloader');
const Adjuster = require('./adjuster');

AFRAME.registerSystem ('osm-data-3d', {

    init: function() {
        this.tilesLoaded = [];
        this.osmLoader = new OsmLoader(this);
        this.adjuster = new Adjuster();
    },

    loadData: async function(url, dem) {
        const tileIndex = `${dem.tile.z}/${dem.tile.x}/${dem.tile.y}`;
        if(this.tilesLoaded.indexOf(tileIndex) == -1) {
            const realUrl = url.replace('{x}', dem.tile.x)
                                .replace('{y}', dem.tile.y)
                                .replace('{z}', dem.tile.z);
            const response = await fetch(realUrl);
            const osmData = await response.json();
            this.tilesLoaded.push(tileIndex);
            return osmData;
        }
        return null;
    },

    applyDem: async function(osmData, dem) {
        await this.osmLoader.loadOsm(osmData,`${dem.tile.z}/${dem.tile.x}/${dem.tile.y}`, dem.dem);
    }

});

AFRAME.registerComponent ('osm-data-3d', {
    schema: {
        url: { type: 'string' }
    },

    init:function() {
        this.el.sceneEl.addEventListener('demloaded',  async(e)=> {
            this.system.adjuster.yExag = e.detail.yExag;
            for(let i=0; i<e.detail.demData.length; i++) {
                const osmData = await this.system.loadData(this.data.url, e.detail.demData[i]);
                await this.system.applyDem(osmData, e.detail.demData[i]);
            }
        });
    },
});
