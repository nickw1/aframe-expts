const OsmWay = require('./osmway');
const JsonTiler = require('./jsontiler');

AFRAME.registerSystem ('osm-data', {

    drawProps: { 'footway' : { color: '#ffff00', width: 5 },
             'path' : { color: '#ffff00', width: 5 },
             'steps' : { color: '#ffff00', width: 5 },
             'bridleway' : { color: '#ff8080', width: 5 },
             'byway' : { color: '#ff8080', width: 10 },
            'track' :  { color: '#ff8080', width: 10 },
            'cycleway' : { color: '#00ffff', width: 5 },
            'residential' : { width: 10 },
            'unclassified' : { width: 15 },
            'tertiary' :  { width: 15 },
            'secondary' : { width: 20 },
            'primary' : { width : 30 },
            'trunk' : { width: 30 },
            'motorway' : { width: 60 }
        },

    init: function() {
        this.tilesLoaded = [];
        this.tiler = new JsonTiler();
    },

    setTilerUrl: function(url) {
        this.tiler.url = url;
    },

    setTilerZoom: function(zoom) {
        this.tiler.setZoom(zoom);
    },

    setDefaultElevation(defaultElevation) {
        this.defaultElevation = defaultElevation;
    },

    updateLonLat: async function(lon, lat) {
        const newData = await this.tiler.updateLonLat(lon, lat);
        for(let k in newData) {
            this.loadOsmData(k, newData[k]);
        }
    },

    lonLatToCamera: function(lon,lat) {
        return this.sphMercToCamera(
            this.tiler.projectLonLat(lon, lat)
        );
    },

    sphMercToCamera: function(p) {
        return { x: p[0],
                y: this.defaultElevation, 
                z: -p[1]
            };
    },

    loadOsmData: async function(tileid, osmData) {
        if(osmData !== null) {
            osmData.features.forEach  ( (f,i)=> {
                const line = [];
                if(f.geometry.type=='LineString' && 
                    f.geometry.coordinates.length >= 2) {
                        f.geometry.coordinates.forEach (coord=> {
                            line.push([ coord[0], 
                                0,
                                -coord[1] ]);
                        });
                    
                
                       const g = this.makeWayGeom(line,     
                        (this.drawProps[f.properties.highway] ? 
                            (this.drawProps[f.properties.highway].width || 5) :
                         5));

                    const color = this.drawProps[f.properties.highway] ? 
                    (this.drawProps[f.properties.highway].color||'#ffffff') : 
                    '#ffffff';
            
                    const mesh = new THREE.Mesh(g, new THREE.MeshBasicMaterial(
                    {color:color}
                    ));
                        
                    this.el.setObject3D(`${tileid}:${f.properties.osm_id}`, mesh);
            }
            }); 
        }
    },

    makeWayGeom: function (vertices, width=1) {
        return new OsmWay(vertices, width).geometry;
    }
});

AFRAME.registerComponent ('osm-data', {
    schema: {
        url: { type: 'string' },
        cameraId: { type :'string' },
        lat: { type: 'number' },
        lon: { type: 'number' },
        zoom: { type: 'int' },
        defaultElevation: { type: 'number', default: 500 }
    },

    init:function() {
        
    },

    update: async function(oldData) {
        this.system.setTilerUrl(this.data.url);
        this.system.setTilerZoom(this.data.zoom);
        this.system.setDefaultElevation(this.data.defaultElevation);
        if(this.data.lon >= -180 && this.data.lon <= 180 && this.data.lat >= -90 && this.data.lat <= 90) {
            this.system.updateLonLat(this.data.lon, this.data.lat);
            const cameraPos = this.system.lonLatToCamera(this.data.lon, this.data.lat);
            document.getElementById(this.data.cameraId)
                .setAttribute('position', cameraPos);
        }
    }
});
