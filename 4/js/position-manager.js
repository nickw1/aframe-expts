AFRAME.registerComponent('position-manager', {

	schema: {
		demId: { type: 'string' }
	},
	
    init: function() {
        this.lastPosition = { x:0, y:0, z:0 };
		this.el.sceneEl.addEventListener('demloaded', e=> {
			if(e.detail.elevation >= 0) {
				this.el.setAttribute('position', {
					x: this.lastPosition.x,
					y: e.detail.elevation,
					z: this.lastPosition.z
				});
			}
		});
    },
    
    tick: function(time, delta) {

        this.newPosition = this.el.getAttribute('position');

        const dx = this.newPosition.x - this.lastPosition.x;
        const dy = this.newPosition.y - this.lastPosition.y;
        const dz = this.newPosition.z - this.lastPosition.z;
        const dist = Math.sqrt(dx*dx + dy*dy +dz*dz);

		if(document.getElementById(this.data.demId) && !this.terrariumComponent) {
			this.terrariumComponent = document.getElementById(this.data.demId)
				.components['terrarium-dem'];
		}
        if(this.terrariumComponent && dist>=0.1) {
			console.log(`Position: ${this.newPosition.x} ${this.newPosition.y} ${this.newPosition.z}`);			
			const groundElevation = this.terrariumComponent.getElevationFromCameraPos (this.newPosition) 
				+ 1.6*this.terrariumComponent.system.adjuster.yExag;
			if(this.newPosition.y < groundElevation) {
				this.newPosition.y = groundElevation;
				this.el.setAttribute('position', {
					x: this.newPosition.x,
					y: this.newPosition.y,
					z: this.newPosition.z
				});
			}
            this.el.sceneEl.emit('cameramoved', {
                x: this.newPosition.x,     
                y: this.newPosition.y, 
                z: this.newPosition.z
            });
        }

        this.lastPosition = { x: this.newPosition.x, 
                          y: this.newPosition.y, 
                         z: this.newPosition.z }; 
    }
});
