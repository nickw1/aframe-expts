
class Adjuster {
    constructor(tiler, yExag=1, defaultElevation=0) {
        this.tiler = tiler;
        this.yExag = yExag;
        this.defaultElevation = 0;
    }

    lonLatToCamera(lon,lat) {
        return this.sphMercToCamera(
            this.tiler.projectLonLat(lon, lat)
        );
    }

    sphMercToCamera(p) {
        return { x: p[0],
                y: this.defaultElevation * this.yExag,
                z: -p[1]
            };
    }

    cameraToSphMerc(cameraPos) {
        return [cameraPos.x, -(cameraPos.z)];
       }
}

module.exports = Adjuster;
