const Eventable = require('./Eventable');

class CameraControl extends Eventable {

    constructor(camera, dist=1) {
        super();
        this.mouseDown = false;
        this.camera = camera;
        this.xvel = 0;
        this.yvel = 0;
        this.zvel = 0;
        this.dist = dist;
    }
    

    setup() {
        document.addEventListener("keydown", this.onkeydown.bind(this));
        document.addEventListener('keyup', this.onkeyup.bind(this));
        document.addEventListener('mousedown', e=> {
            this.mouseDown = true;
        });
        document.addEventListener('mouseup', e=> {
            this.mouseDown = false;
        });
        document.addEventListener("mousemove", this.onmousemove.bind(this));
    }

    onkeydown(e) {
        switch(e.keyCode) {
            case 87: 
            case 38:
                this.zvel =  -this.dist*Math.cos(this.camera.rotation.y); 
                this.xvel =  -this.dist*Math.sin(this.camera.rotation.y); 
                break;
            case 83:
            case 40:
                this.zvel =  this.dist*Math.cos(this.camera.rotation.y); 
                this.xvel =  this.dist*Math.sin(this.camera.rotation.y); 
                break;
            case 65: 
            case 37:
                this.xvel =  -this.dist*Math.cos(this.camera.rotation.y); 
                this.zvel =  this.dist*Math.sin(this.camera.rotation.y); 
                break;
            case 68: 
            case 39:
                this.xvel =  this.dist*Math.cos(this.camera.rotation.y); 
                this.zvel =  -this.dist*Math.sin(this.camera.rotation.y); 
                break;
            case 81: this.yvel = this.dist; break;
            case 90: this.yvel = -this.dist; break;
        }
    }

    onkeyup(e) {
        switch(e.keyCode) {
            case 87: 
            case 83: 
            case 65: 
            case 68: 
            case 37:
            case 38: 
            case 39: 
            case 40: 
                this.xvel=0; 
                this.zvel=0; 
                break;
            case 81: 
            case 90: 
                this.yvel=0;
                break;
        }
    }

    onmousemove(e) {    
        if(this.mouseDown){
            this.camera.rotation.y += e.movementX*0.001;
        
            if(this.camera.rotation.y > Math.PI) {
                this.camera.rotation.y -= Math.PI*2;
            }
            else if(this.camera.rotation.y < -Math.PI) {
                this.camera.rotation.y += Math.PI*2;
            }
        
        }
    }

    moveCamera() {
        const h = this.eventHandlers.cameramove ? 
            this.eventHandlers.cameramove(this.camera.position) : - 1;
        this.camera.position.x  += this.xvel;
        this.camera.position.y  += this.yvel;
        this.camera.position.z  += this.zvel;
        if(this.camera.position.y < h) this.camera.position.y = h + 4.8;
        if(this.eventHandlers.cameramoved!==null && 
            (Math.abs(this.xvel)>0 || 
             Math.abs(this.yvel)>0 || 
            Math.abs(this.zvel)>0)) {
                this.eventHandlers.cameramoved({x:this.camera.position.x,
                                y: this.camera.position.y,
                                z: this.camera.position.z});
        }
    }
}

module.exports = CameraControl;
