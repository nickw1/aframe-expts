const OsmLoader = require('./osmloader');
const JsonTiler = require('./jsontiler');
const Adjuster = require('./adjuster');

AFRAME.registerSystem ('osm-data', {

    init: function() {
        this.tilesLoaded = [];
        this.tiler = new JsonTiler();
        this.adjuster = new Adjuster(this.tiler);
        this.osmLoader = new OsmLoader(this);
    },

    setTilerUrl: function(url) {
        this.tiler.url = url;
    },

    setTilerZoom: function(zoom) {
        this.tiler.setZoom(zoom);
    },

    setDefaultElevation(defaultElevation) {
        this.adjuster.defaultElevation = defaultElevation;
    },

    updateLonLat: async function(lon, lat) {
        const sphMerc = this.tiler.lonLatToSphMerc(lon,lat);
        return await this.updateSphMerc(sphMerc);
    },

    updateSphMerc: async function(sphMerc) {
        const newData = await this.tiler.update(sphMerc);
           newData.forEach ( data=> { 
            this.loadOsmData(data.data, `${data.tile.z}/${data.tile.x}/${data.tile.y}`);
        })
    },

    loadOsmData: async function(tileid, osmData) {
        if(osmData !== null) {
            this.osmLoader.loadOsm(tileid, osmData);
        }
    },
});

AFRAME.registerComponent ('osm-data', {
    schema: {
        url: { type: 'string' },
        cameraId: { type :'string' },
        lat: { type: 'number' },
        lon: { type: 'number' },
        zoom: { type: 'int' },
        defaultElevation: { type: 'number', default: 500 }
    },

    init:function() {
        this.el.sceneEl.addEventListener("cameramoved", e=> {
            const sphMerc = this.system.adjuster.cameraToSphMerc(e.detail);
            this.system.updateSphMerc(sphMerc);
        });        
    },

    update: async function(oldData) {
        this.system.setTilerUrl(this.data.url);
        this.system.setTilerZoom(this.data.zoom);
        this.system.setDefaultElevation(this.data.defaultElevation);
        if(this.data.lon >= -180 && this.data.lon <= 180 && this.data.lat >= -90 && this.data.lat <= 90) {
            this.system.updateLonLat(this.data.lon, this.data.lat);
            const cameraPos = this.system.adjuster.lonLatToCamera(this.data.lon, this.data.lat);
            document.getElementById(this.data.cameraId)
                .setAttribute('position', cameraPos);
        }
    }
});
