const OsmWay = require('./osmway');
const DemTiler = require('./demtiler');
const Adjuster = require('./adjuster');
const DEM = require('./dem');

AFRAME.registerSystem ('terrarium-dem', {

    init: function() {
        this.tilesLoaded = [];
        this.tiler = new DemTiler();
        this.adjuster = new Adjuster(this.tiler);
        this.dems = {};
        this.firstLoad = true;
    },

    setTilerUrl: function(url) {
        this.tiler.url = url;
    },

    setTilerZoom: function(zoom) {
        this.tiler.setZoom(zoom);
    },

    setParams: function(yExag) {
        this.adjuster.yExag = yExag;
    },

    setDefaultElevation(defaultElevation) {
        this.adjuster.defaultElevation = defaultElevation;
    },

    updateLonLat: async function(lon, lat) {
        const sphMerc = this.tiler.lonLatToSphMerc(lon,lat);
        return await this.updateSphMerc(sphMerc);
    },

    updateSphMerc: async function(sphMerc) {
        const dems = [];
        const newData = await this.tiler.update(sphMerc);
        newData.forEach ( data=> { 
            const dem = this.loadTerrariumData(data);
            if(dem != null) {
                dems.push(dem);
            }
        });     
        return dems;
    },

    loadTerrariumData: function(data) {
        let demData = null;    
        if(data !== null) {
            const geom = this.createDemGeometry(data);
            geom.geom.computeFaceNormals();
            geom.geom.computeVertexNormals();
            const mesh = new THREE.Mesh(geom.geom, new THREE.MeshLambertMaterial({
                color: this.color,
                opacity: this.opacity
            }));
            const demEl = document.createElement('a-entity');
            demEl.setObject3D('mesh', mesh);
            this.el.appendChild(demEl);
            const dem = new DEM(geom.geom, geom.realBottomLeft);
            this.dems[`${data.tile.z}/${data.tile.x}/${data.tile.y}`] = dem;
            demData = { dem: dem, tile: data.tile};
        }
        return demData;
    },

    createDemGeometry: function(data) {
        const demData = data.data;
        const tile = data.tile; 
        const topRight = tile.getTopRight();
        const bottomLeft = tile.getBottomLeft();
        const centre = [(topRight[0] + bottomLeft[0]) / 2, 
            (topRight[1] + bottomLeft[1]) /2];
        const xSpacing = (topRight[0] - bottomLeft[0]) / (demData.w-1);
        const ySpacing = (topRight[1] - bottomLeft[1]) / (demData.h-1);
        const realBottomLeft = [bottomLeft[0], bottomLeft[1]] ;
        const geom = new THREE.PlaneBufferGeometry(topRight[0] - bottomLeft[0], topRight[1] - bottomLeft[1], demData.w - 1,  demData.h - 1);
        const array = geom.getAttribute("position").array;
        let i;
        for (let row=0; row<demData.h; row++) {
           for(let col=0; col<demData.w; col++) {
                i = row*demData.w + col;
                array[i*3+2] = -(centre[1] + array[i*3+1]); 
                array[i*3+1] = demData.elevs[i] * this.adjuster.yExag;
                array[i*3] += centre[0];
            }        
        }

        return {geom: geom, realBottomLeft: realBottomLeft };    
    },

    getElevation: function(lon, lat, z) {
        const sphMercPos = this.tiler.lonLatToSphMerc(lon, lat, z);
        return this.getElevationFromSphMerc(sphMercPos, z);
    },

    getElevationFromSphMerc: function(sphMercPos, z) {    
        const tile = this.tiler.getTile(sphMercPos, z);
        if(this.dems[`${tile.z}/${tile.x}/${tile.y}`]) {
            const scaled = [ sphMercPos[0], sphMercPos[1]  ];
            return this.dems[`${tile.z}/${tile.x}/${tile.y}`].getHeight
                (scaled[0], scaled[1]);
        }
        return -1;
    }


});

AFRAME.registerComponent ('terrarium-dem', {
    schema: {
        url: { type: 'string' },
        cameraId: { type :'string' },
        lat: { type: 'number', default: -91 },
        lon: { type: 'number', default: -181 },
        zoom: { type: 'int' },
        defaultElevation: { type: 'number', default: 500 },
        yExag: { type: 'number', default: 3},
        color: { type: 'color', default:'#00c000' },
        opacity: { type: 'number', default: 1.0 }    
    },

    init:function() {
        this.el.sceneEl.addEventListener("cameramoved", async(e)=> {
            if(!this.system.firstLoad) {
                document.getElementById(this.data.dialogId).style.visibility = 'visible';
                const sphMerc = this.system.adjuster.cameraToSphMerc(e.detail);
                const demData = await this.system.updateSphMerc(sphMerc);
                // demData: array of { dem, tile } objects
                this.notifyDemLoaded(demData, false);
                document.getElementById(this.data.dialogId).style.visibility = 'hidden';
            }
        });        
    },

    update: async function(oldData) {
        this.system.setTilerUrl(this.data.url);
        this.system.setTilerZoom(this.data.zoom);
        this.system.color = this.data.color;
        this.system.opacity = this.data.opacity;
        this.system.setDefaultElevation(this.data.defaultElevation);
        this.system.setParams(this.data.yExag);
        if(this.data.lon >= -180 && this.data.lon <= 180 && this.data.lat >= -90 && this.data.lat <= 90) {
            const cameraPos = this.system.adjuster.lonLatToCamera(this.data.lon, this.data.lat);
            document.getElementById(this.data.cameraId)
                .setAttribute('position', cameraPos);
            document.getElementById(this.data.dialogId).style.visibility = 'visible';
            const demData = await this.system.updateLonLat(this.data.lon, this.data.lat);
            document.getElementById(this.data.dialogId).style.visibility = 'hidden';
            this.system.firstLoad = false;
            this.notifyDemLoaded(demData, true);
        }
    },

    notifyDemLoaded: function(demData, moveToGround) {
        this.el.sceneEl.emit('demloaded', {
            demData: demData,
            yExag: this.system.adjuster.yExag,
            elevation: moveToGround ? this.system.getElevation(this.data.lon, this.data.lat, this.data.zoom)+10*this.system.adjuster.yExag: -1 
        });
    },


    getElevationFromCameraPos: function(cameraPos) {
        const sphMercPos = this.system.adjuster.cameraToSphMerc(cameraPos);
        const h = this.system.getElevationFromSphMerc(sphMercPos, this.data.zoom);
        return h;
    }
});
