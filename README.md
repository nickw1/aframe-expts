aframe-expts
============

Experiments with A-Frame and OSM data, in preparation for working on AR.js.
Four experiments of increasing complexity.

Also contains a pure three.js version of experiment 4, to get familiar with
raw three concepts.
